# INTRO #

The API is provided to query the data from a neaboard server by any client
application.

Tha data is returned in the json format and got by an http query.

# METHODS #

## Get post URL ##
May be used for parsing purposes (board call parser and parser determines there
is a reflink which should be parsed as a post URL).

Should return 404 if there is no corresponding post.

    /api/get-post-url/<id>

## Service API for creating posts ##

This API call is designed for news bots to post into threads

    /api/service/add-post
    
Accepts JSON:

    {
        'thread': 123,
        'title': 'post title',
        'text': 'post raw text',
        'tripcode': 'tripcode source string',
        'urls': []
    }
    
For now only inserting posts into threads is supported.

X-API-SECRET header should be specified with a key configured in server
properties for security reasons.