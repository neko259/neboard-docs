# 0 Title #

"post" model reference of version 1.1

# 1 Description #

"post" is a model that defines an imageboard message, or post.

# 2 Fields #

# 2.1 Mandatory fields #

* title -- text field.
* text -- text field.
* pub-time -- timestamp (TBD: Define format).
* update -- when post content changes, the update time should be incremented.

# 2.2 Optional fields #

* edit-time -- timestamp (TBD: Define format).
* attachments -- defines attachments such as images.
* attachment -- contains and attachment or link to attachment to be downloaded
manually. Required attributes are mimetype and name.

This field is used for the opening post (thread):

* tags -- text tag name list.

This field is used for non-opening post:

* thread -- ID of a post model that this post is related to.
* tripcode
