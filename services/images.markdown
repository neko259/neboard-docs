# API for images service #

# INTRO #
Image service does not store the images by itself. It only returns file names
of the files in file service. Metadata can be retrieved from the file service
as well

# METHODS #
## Get domain image metadata ##

    /domains/image/<domain>

Finds the image for specified domain including higher levels. E.g. if ru.wikipedia.org is specified, first image for
ru.wikipedia.org is looked for, and then wikipedia.org if the first one is not present.

Returns json:

    {
        'name': 'myimage.png',
    }
    
## Get file format metadata ##

    /files/image/<mimetype>
    
Returns an image representing file mimetype, if any.
    
Returns json:

    {
        'name': 'myimage.png'
    }

## Find stickers ##

    /stickers/<query>
    
Finds stickers matching the following query for both sticker pack name and sticker name.

Returns json list:

    [
        {
            'name': 'image.png'
        }
    ]
    
## Get sticker by name ##

    /sticker/<name>
    
Get the specific sticker url for download

    {
        'name': 'image.png'
    }
    
## Last update for domain images ##

    /domains/last-update

Gets the last update date of all domains. Used for caching on the client side. Format is arbitrary, clients check
for equality and use it as a global cache key.

    {
        'time': '12345'
    }