# API for parser service #

# METHODS #
## Available parse methods list ##

    /list/
    
Currently available methods are *simple* and *bbcode*.
    
## Preparse text ##

    /preparse/<method>
    
Method is the selected parsing methods
    
Body:
* raw_text
* post_mapping

## Get reflinks list ##

Method will gather reflinks for the board to process.

Body:
* raw_text

Returns

    {
        'reflinks': ['1', '2', '3']
    }


## Parse text ##

    /parse/<method>
    
Method is the selected parsing methods
    
Body:
* raw_text
* post_mapping

Board url is required for parsing post reflinks

Returns json with following structure:

    {
        'text': 'parsed text',
    }
    
    
Reflinks field is optional and is populated when there were links to other posts parsed inside a post text.

Post mapping is a json with the following structure:

    {
        '1': {
            'url': '/thread/1#2',
            'opening': 'false'
        }
    }
    
## Get tags ##

    /tags/<method>
    
Returns a list of tag definitions to be diplayed as a markup panel.

    [
        {
            "name": "bold",
            "previewLeft": "<b>",
            "previewRight": "</b>",
            "tagLeft": "[b]",
            "tagRight": "[/b]",
            "needsInput": false,
            "inputHint": null
        }
    ]